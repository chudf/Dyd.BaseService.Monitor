﻿using BSF.Config;
using Monitor.Domain.TimeWatch.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitor.Tasks
{
    /// <summary>
    /// Api耗时采集统计任务
    /// 建议cron： 1 0/5 * * * ?
    /// 描述:用于统计Api性能监控数据，包括平均访问时间，最好耗时，最小耗时等。
    /// </summary>
    public class TimeWatchApiCollectTask : BSF.BaseService.TaskManager.BaseDllTask
    {
        public override void Run()
        {
            try
            {
                InitConfig();
                Do();
            }
            catch (Exception exception)
            {
                base.OpenOperator.Error("性能监控定时统计API性能失败", exception);
            }
        }

        public void InitConfig()
        {
            BSFConfig.MonitorPlatformConnectionString = base.AppConfig["MonitorPlatformConnectionString"];
        }

        private void Do()
        {
            tb_timewatchlog_api_dayreport_dal Dal = new tb_timewatchlog_api_dayreport_dal();
            bool c = Dal.StatisApiMinitor();
            if (c)
            {
                base.OpenOperator.Log("性能监控定时统计API性能成功");
            }
        }

        public override void TestRun()
        {
            /*测试环境下任务的配置信息需要手工填写,正式环境下需要配置在任务配置中心里面*/
            base.AppConfig = new BSF.BaseService.TaskManager.SystemRuntime.TaskAppConfigInfo();
            base.AppConfig.Add("MonitorPlatformConnectionString", "server=192.168.17.201;Initial Catalog=dyd_bs_monitor_platform_manage;User ID=sa;Password=Xx~!@#;");
            base.TestRun();
        }
    }
}
